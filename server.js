//express เป็น library
const express = require('express') 

//สร้างappของlibrary
const app = express()

let todos =[
    {
        name : 'ploy',
        id :1
    },
    {
        name :'chanoksuda',
        id : 2
    }
]


//สร้างapi ที่มี method เป็นget เทียบเป็น(select * from todo)
//ระบุ app ตามด้วย action' ชื่อresouce, ตามด้วยฟังก์ชัน
app.get('/todos',(req,res) => {
    res.send(todos)

})

app.post('/todos',(req,res) => {
    let newTodo = {
        name : 'Read a book',
        id : 3
    }

    todos.push(newTodo) //เพิ่มข้อมูล
    res.status(201).send() //ตอบกลับ
    

})

app.listen(3000, () => {
    console.log('TODO API Started at port 3000')
})